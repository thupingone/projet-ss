# Projet SS

Le script doit être exécuté de la manière suivante :

-Ouvrir un terminal
-Se placer dans le répertoire du projet.
-exécuter la commande « bash LeScript.sh »
-Attendre que le menu s'affiche
-Choisir la commande à exécuter !
-Attention : 
    Il faut choisir des numéro pour selectioner le menu
    Dans "2) - Etude de la consommation d'un pays/continent par année -" il faut selectioner un pays ou continent selon la liste ci-dessous
    Dans "3) - Production d'une énergie renouvelable en TWh -" Il faut selectionner l'energie en tout lettre selon la liste affiché
    Dans "4) - Comparaison de l'évolution des énergies renouvelables par année -" Il faut selectionner l'energie en nombre selon la liste affiché


Liste pays et continents disponible :
- Continents : World,OECD,BRICS,Europe,North America,Latin America,Asia,Pacific,Africa,Middle-East,CIS
- Pays : China,United States,Brazil,Belgium,Czechia,France,Germany,Italy,Netherlands,Poland,Portugal,Romania,Spain,Sweden,United Kingdom,Norway,Turkey,Kazakhstan,Russia,Ukraine,Uzbekistan,Argentina,Canada,Chile,Colombia,Mexico,Venezuela,Indonesia,Japan,Malaysia,South Korea,Taiwan,Thailand,India,Australia,New Zealand,Algeria,Egypt,Nigeria,South Africa,Iran,Kuwait,Saudi Arabia,United Arab Emirates.
